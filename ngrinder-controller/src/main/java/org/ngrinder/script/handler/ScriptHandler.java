/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.script.handler;

<<<<<<< HEAD
import static org.apache.commons.lang.StringUtils.startsWithIgnoreCase;
import static org.ngrinder.common.util.CollectionUtils.newArrayList;
import static org.ngrinder.common.util.ExceptionUtils.processException;

import java.io.File;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.ngrinder.common.util.FileUtil;
=======
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import org.apache.commons.io.FilenameUtils;
import org.ngrinder.common.constant.ControllerConstants;
import org.ngrinder.common.util.FileUtils;
import org.ngrinder.common.util.PathUtils;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.common.util.PropertiesWrapper;
import org.ngrinder.model.User;
import org.ngrinder.script.model.FileEntry;
import org.ngrinder.script.model.FileType;
import org.ngrinder.script.repository.FileEntryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;

<<<<<<< HEAD
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
=======
import java.io.File;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang.StringUtils.startsWithIgnoreCase;
import static org.ngrinder.common.util.CollectionUtils.newArrayList;
import static org.ngrinder.common.util.ExceptionUtils.processException;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

/**
 * Script per language handler. This is the superclass for all sub
 * {@link ScriptHandler}s which implements the specific processing of each
 * language.
<<<<<<< HEAD
 * 
 * @author JunHo Yoon
 * @since 3.2
 */
public abstract class ScriptHandler {
=======
 *
 * @author JunHo Yoon
 * @since 3.2
 */
public abstract class ScriptHandler implements ControllerConstants {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	protected static final Logger LOGGER = LoggerFactory.getLogger(JythonScriptHandler.class);
	private final String codemirrorKey;
	private final String title;
	private final String extension;
	private final String key;

	/**
	 * Constructor.
<<<<<<< HEAD
	 * 
	 * @param key
	 *            key of the script handler
	 * @param extension
	 *            extension
	 * @param title
	 *            title of the handler
	 * @param codemirrorKey
	 *            code minrror key
	 */
	public ScriptHandler(String key, String extension, String title, String codemirrorKey) {
		this.key = key;
		this.extension = extension;
		this.title = title;
		this.codemirrorKey = codemirrorKey;
=======
	 *
	 * @param key           key of the script handler
	 * @param extension     extension
	 * @param title         title of the handler
	 * @param codeMirrorKey code mirror key
	 */
	public ScriptHandler(String key, String extension, String title, String codeMirrorKey) {
		this.key = key;
		this.extension = extension;
		this.title = title;
		this.codemirrorKey = codeMirrorKey;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	@Autowired
	private FileEntryRepository fileEntryRepository;

	/**
	 * Get the display order of {@link ScriptHandler}s.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return order
	 */
	public abstract Integer displayOrder();

	public String getCodemirrorKey() {
		return codemirrorKey;
	}

	/**
	 * Check if the given fileEntry can be handled by this handler.
<<<<<<< HEAD
	 * 
	 * @param fileEntry
	 *            fileEntry to be checked
	 * @return true if handleable
=======
	 *
	 * @param fileEntry fileEntry to be checked
	 * @return true if the given fileEntry can be handled
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public boolean canHandle(FileEntry fileEntry) {
		return FilenameUtils.isExtension(fileEntry.getPath(), getExtension());
	}

	public String getExtension() {
		return extension;
	}

	/**
	 * Get the handler resolution order.
<<<<<<< HEAD
	 * 
	 * Less is more prioritized.
	 * 
=======
	 * <p/>
	 * Less is more prioritized.
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return the order of handler resolution
	 */
	protected abstract Integer order();

<<<<<<< HEAD
=======
	@SuppressWarnings("SpellCheckingInspection")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public boolean isValidatable() {
		return true;
	}

	/**
	 * Return if it's project handler which implements {@link ProjectHandler}.
<<<<<<< HEAD
	 * 
	 * @return true if it is.
	 */
=======
	 *
	 * @return true if it is.
	 */

	@SuppressWarnings("UnusedDeclaration")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public boolean isProjectHandler() {
		return (this instanceof ProjectHandler);
	}

	/**
	 * Prepare the distribution.
<<<<<<< HEAD
	 * 
	 * @param testcaseId
	 *            id of the test case. This is for the log identification.
	 * @param user
	 *            user who will distribute the script.
	 * @param scriptEntry
	 *            script to be distributed.
	 * @param distDir
	 *            distribution target dir
	 * @param properties
	 *            properties set which is used for detailed distribution control
	 * @param processingResult
	 *            processing result holder.
	 */
	public void prepareDist(Long testcaseId,
			User user, //
			FileEntry scriptEntry, File distDir, PropertiesWrapper properties,
			ProcessingResultPrintStream processingResult) {
=======
	 *
	 * @param testCaseId       id of the test case. This is for the log identification.
	 * @param user             user who will distribute the script.
	 * @param scriptEntry      script to be distributed.
	 * @param distDir          distribution target dir.
	 * @param properties       properties set which is used for detailed distribution control.
	 * @param processingResult processing result holder.
	 */
	public void prepareDist(Long testCaseId,
	                        User user, //
	                        FileEntry scriptEntry, File distDir, PropertiesWrapper properties,
	                        ProcessingResultPrintStream processingResult) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		prepareDefaultFile(distDir, properties);
		List<FileEntry> fileEntries = getLibAndResourceEntries(user, scriptEntry, -1);
		if (scriptEntry.getRevision() != 0) {
			fileEntries.add(scriptEntry);
		}
		String basePath = getBasePath(scriptEntry);
		// Distribute each files in that folder.
		for (FileEntry each : fileEntries) {
			// Directory is not subject to be distributed.
			if (each.getFileType() == FileType.DIR) {
				continue;
			}
			File toDir = new File(distDir, calcDistSubPath(basePath, each));
			processingResult.printf("%s is being written.\n", each.getPath());
<<<<<<< HEAD
			LOGGER.info("{} is being written in {} for test {}", new Object[] { each.getPath(), toDir, testcaseId });
			getFileEntryRepository().writeContentTo(user, each.getPath(), toDir);
		}
		processingResult.setSuccess(true);
		prepareDistMore(testcaseId, user, scriptEntry, distDir, properties, processingResult);
=======
			LOGGER.info("{} is being written in {} for test {}", new Object[]{each.getPath(), toDir, testCaseId});
			getFileEntryRepository().writeContentTo(user, each.getPath(), toDir);
		}
		processingResult.setSuccess(true);
		prepareDistMore(testCaseId, user, scriptEntry, distDir, properties, processingResult);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Prepare script creation. This method is subject to be extended by the
	 * subclasses.
<<<<<<< HEAD
	 * 
	 * This method is the perfect place if it's necessary to include additional
	 * files.
	 * 
	 * @param user
	 *            user
	 * @param path
	 *            base path
	 * @param fileName
	 *            fileName
	 * @param name
	 *            name
	 * @param url
	 *            url
	 * @param createLibAndResources
	 *            true if lib and resources should be created
	 * 
	 * @return true if process more.
	 */
	public boolean prepareScriptEnv(User user, String path, String fileName, String name, String url,
			boolean createLibAndResources) {
=======
	 * <p/>
	 * This method is the perfect place if it's necessary to include additional
	 * files.
	 *
	 * @param user                  user
	 * @param path                  base path
	 * @param fileName              fileName
	 * @param name                  name
	 * @param url                   url
	 * @param createLibAndResources true if lib and resources should be created
	 * @return true if process more.
	 */
	public boolean prepareScriptEnv(User user, String path, String fileName, String name, String url,
	                                boolean createLibAndResources) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return true;
	}

	/**
	 * Prepare the distribution more. This method is subject to be extended by
	 * the subclass.
<<<<<<< HEAD
	 * 
	 * @param testcaseId
	 *            testcase id. This is for the log identification.
	 * @param user
	 *            user
	 * @param script
	 *            script entry to be distributed.
	 * @param distDir
	 *            distribution directory
	 * @param properties
	 *            properties.
	 * @param processingResult
	 *            processing result holder
	 */
	protected void prepareDistMore(Long testcaseId, User user, FileEntry script, File distDir,
			PropertiesWrapper properties, ProcessingResultPrintStream processingResult) {
=======
	 *
	 * @param testCaseId       test case id. This is for the log identification.
	 * @param user             user
	 * @param script           script entry to be distributed.
	 * @param distDir          distribution directory
	 * @param properties       properties
	 * @param processingResult processing result holder
	 */
	protected void prepareDistMore(Long testCaseId, User user, FileEntry script, File distDir,
	                               PropertiesWrapper properties, ProcessingResultPrintStream processingResult) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the appropriated distribution path for the given file entry.
<<<<<<< HEAD
	 * 
	 * @param basePath
	 *            distribution base path
	 * @param fileEntry
	 *            fileEntry to be distributed
=======
	 *
	 * @param basePath  distribution base path
	 * @param fileEntry fileEntry to be distributed
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return the resolved destination path.
	 */
	protected String calcDistSubPath(String basePath, FileEntry fileEntry) {
		String path = FilenameUtils.getPath(fileEntry.getPath());
		path = path.substring(basePath.length());
		return path;
	}

	/**
	 * Get all resources and lib entries belonging to the given user and
	 * scriptEntry.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            user
	 * @param scriptEntry
	 *            script entry
	 * @param revision
	 *            revision of the script entry.
=======
	 *
	 * @param user        user
	 * @param scriptEntry script entry
	 * @param revision    revision of the script entry.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return file entry list
	 */
	public List<FileEntry> getLibAndResourceEntries(User user, FileEntry scriptEntry, long revision) {
		String path = FilenameUtils.getPath(scriptEntry.getPath());
		List<FileEntry> fileList = newArrayList();
		for (FileEntry eachFileEntry : getFileEntryRepository().findAll(user, path + "lib/", revision, true)) {
			// Skip jython 2.5... it's already included.
			if (startsWithIgnoreCase(eachFileEntry.getFileName(), "jython-2.5.")
					|| startsWithIgnoreCase(eachFileEntry.getFileName(), "jython-standalone-2.5.")) {
				continue;
			}
			FileType fileType = eachFileEntry.getFileType();
<<<<<<< HEAD
			if (fileType.isLibDistribtable()) {
=======
			if (fileType.isLibDistributable()) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				fileList.add(eachFileEntry);
			}
		}
		for (FileEntry eachFileEntry : getFileEntryRepository().findAll(user, path + "resources/", revision, true)) {
			FileType fileType = eachFileEntry.getFileType();
			if (fileType.isResourceDistributable()) {
				fileList.add(eachFileEntry);
			}
		}
		return fileList;
	}

	protected void prepareDefaultFile(File distDir, PropertiesWrapper properties) {
<<<<<<< HEAD
		if (properties.getPropertyBoolean("ngrinder.dist.logback", true)) {
			FileUtil.copyResourceToFile("/logback/logback-worker.xml", new File(distDir, "logback-worker.xml"));
=======
		if (properties.getPropertyBoolean(PROP_CONTROLLER_DIST_LOGBACK)) {
			FileUtils.copyResourceToFile("/logback/logback-worker.xml", new File(distDir, "logback-worker.xml"));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}

	protected String getBasePath(FileEntry script) {
		return getBasePath(script.getPath());
	}

	/**
	 * Get the base path of the given path.
<<<<<<< HEAD
	 * 
	 * @param path
	 *            path
=======
	 *
	 * @param path path
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return base path
	 */
	public String getBasePath(String path) {
		return FilenameUtils.getPath(path);
	}

	/**
	 * Get executable script path.
<<<<<<< HEAD
	 * 
	 * @param svnPath
	 *            path in svn
=======
	 *
	 * @param svnPath path in svn
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return path executable in agent.
	 */
	public String getScriptExecutePath(String svnPath) {
		return FilenameUtils.getName(svnPath);
	}

	/**
	 * Check syntax errors for the given content.
<<<<<<< HEAD
	 * 
	 * @param path
	 *            path
	 * @param content
	 *            content
=======
	 *
	 * @param path    path
	 * @param content content
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return syntax error messages. null if none.
	 */
	public abstract String checkSyntaxErrors(String path, String content);

	/**
	 * Get the initial script with the given value map.
<<<<<<< HEAD
	 * 
	 * @param values
	 *            map of initial script referencing values.
=======
	 *
	 * @param values map of initial script referencing values.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return generated string
	 */
	public String getScriptTemplate(Map<String, Object> values) {
		try {
			Configuration freemarkerConfig = new Configuration();
			ClassPathResource cpr = new ClassPathResource("script_template");
			freemarkerConfig.setDirectoryForTemplateLoading(cpr.getFile());
			freemarkerConfig.setObjectWrapper(new DefaultObjectWrapper());
			Template template = freemarkerConfig.getTemplate("basic_template_" + getExtension() + ".ftl");
			StringWriter writer = new StringWriter();
			template.process(values, writer);
			return writer.toString();
		} catch (Exception e) {
			throw processException("Error while fetching the script template.", e);
		}
	}

	public String getTitle() {
		return title;
	}

	public String getKey() {
		return key;
	}

	FileEntryRepository getFileEntryRepository() {
		return fileEntryRepository;
	}

	void setFileEntryRepository(FileEntryRepository fileEntryRepository) {
		this.fileEntryRepository = fileEntryRepository;
	}

	/**
	 * Get the default quick test file.
<<<<<<< HEAD
	 * 
	 * @param basePath
	 *            base path
=======
	 *
	 * @param basePath base path
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return quick test file
	 */
	public FileEntry getDefaultQuickTestFilePath(String basePath) {
		FileEntry fileEntry = new FileEntry();
<<<<<<< HEAD
		fileEntry.setPath(basePath + "/script." + getExtension());
=======
		fileEntry.setPath(PathUtils.join(basePath, "TestRunner." + getExtension()));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return fileEntry;
	}
}
