/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.perftest.service;

import java.util.List;

import org.ngrinder.model.PerfTest;
import org.ngrinder.model.Status;
import org.springframework.transaction.annotation.Transactional;

/**
<<<<<<< HEAD
 * {@link PerfTest} Service Class.
 * 
 * This class contains various method which mainly get the {@link PerfTest} matching specific
 * conditions from DB.
=======
 * {@link PerfTest} Service Class for cluster mode.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * 
 * @author JunHo Yoon
 * @author Mavlarn
 * @since 3.0
 */
public class ClusteredPerfTestService extends PerfTestService {
	/**
<<<<<<< HEAD
	 * Get next runnable PerfTest list.
	 * 
	 * @return found {@link PerfTest} which is ready to run, null otherwise
	 */
	@Transactional
	public PerfTest getPerfTestCandiate() {
		List<PerfTest> readyPerfTests = getPerfTestRepository().findAllByStatusAndRegionOrderByScheduledTimeAsc(
						Status.READY, getConfig().getRegion());
=======
	 * Get next runnable {@link PerfTest}.
	 * 
	 * @return found {@link PerfTest} which is ready to run, null otherwise
	 */

	@Override
	@Transactional
	public PerfTest getNextRunnablePerfTestPerfTestCandidate() {
		List<PerfTest> readyPerfTests = getPerfTestRepository().findAllByStatusAndRegionOrderByScheduledTimeAsc(
				Status.READY, getConfig().getRegion());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		List<PerfTest> usersFirstPerfTests = filterCurrentlyRunningTestUsersTest(readyPerfTests);
		return usersFirstPerfTests.isEmpty() ? null : readyPerfTests.get(0);
	}
}
