/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.user.controller;

<<<<<<< HEAD
import static org.ngrinder.common.util.Preconditions.checkArgument;
import static org.ngrinder.common.util.Preconditions.checkNotEmpty;
import static org.ngrinder.common.util.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.ngrinder.common.controller.NGrinderBaseController;
=======
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.gson.annotations.Expose;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.ngrinder.common.constant.ControllerConstants;
import org.ngrinder.common.controller.BaseController;
import org.ngrinder.common.controller.RestAPI;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.infra.config.Config;
import org.ngrinder.model.Permission;
import org.ngrinder.model.Role;
import org.ngrinder.model.User;
import org.ngrinder.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
<<<<<<< HEAD
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.web.PageableDefaults;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;

/**
 * User management controller.
 * 
 * @author JunHo Yoon
 * @author Alex Quin
 * @since 3.0
 * 
 */
@Controller
@RequestMapping("/user")
public class UserController extends NGrinderBaseController {
=======
import org.springframework.data.web.PageableDefaults;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import static org.ngrinder.common.util.CollectionUtils.newArrayList;
import static org.ngrinder.common.util.ObjectUtils.defaultIfNull;
import static org.ngrinder.common.util.Preconditions.*;

/**
 * User management controller.
 *
 * @author JunHo Yoon
 * @author Alex Quin
 * @since 3.0
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	@Autowired
	private UserService userService;

	@Autowired
<<<<<<< HEAD
	private Config config;

	/**
	 * Get user list on the given role.
	 * 
	 * @param model
	 *            model
	 * @param roleName
	 *            role
	 * @param pageable
	 *            page info
	 * @param keywords
	 *            search keyword.
	 * @return user/userList
	 */
	@PreAuthorize("hasAnyRole('A')")
	@RequestMapping({ "", "/" })
	public String getUserList(ModelMap model, @RequestParam(required = false) String roleName,
					@PageableDefaults(pageNumber = 0, value = 10) Pageable pageable,
					@RequestParam(required = false) String keywords) {

		PageRequest pageReq = ((PageRequest) pageable);
		Sort sort = pageReq == null ? null : pageReq.getSort();
		if (sort == null && pageReq != null) {
			sort = new Sort(Direction.ASC, "userName");
			pageable = new PageRequest(pageReq.getPageNumber(), pageReq.getPageSize(), sort);
		}
		Page<User> pagedUser = null;
		if (StringUtils.isEmpty(keywords)) {
			pagedUser = userService.getAllUserByRole(roleName, pageable);
		} else {
			pagedUser = userService.getUserListByKeyWord(keywords, pageable);
			model.put("keywords", keywords);
		}
		model.addAttribute("userPage", pagedUser);
		EnumSet<Role> roleSet = EnumSet.allOf(Role.class);
		model.addAttribute("roleSet", roleSet);
		model.addAttribute("roleName", roleName);
		model.addAttribute("page", pageable);
		if (sort != null) {
			Order sortProp = (Order) sort.iterator().next();
			model.addAttribute("sortColumn", sortProp.getProperty());
			model.addAttribute("sortDirection", sortProp.getDirection());
		}
		return "user/list";
	}

	/**
	 * Get user creation form page.
	 * 
	 * @param user
	 *            current user
	 * @param model
	 *            mode
	 * @return "user/userDetail"
	 */
	@RequestMapping("/new")
	@PreAuthorize("hasAnyRole('A') or #user.userId == #userId")
	public String getUserDetail(User user, final ModelMap model) {
		model.addAttribute("roleSet", EnumSet.allOf(Role.class));
=======
	protected Config config;

	private ServletContext servletContext;


	/**
	 * Get user list on the given role.
	 *
	 * @param model    model
	 * @param role     role
	 * @param pageable page info
	 * @param keywords search keyword.
	 * @return user/userList
	 */
	@PreAuthorize("hasAnyRole('A')")
	@RequestMapping({"", "/"})
	public String getAll(ModelMap model, @RequestParam(required = false) Role role,
	                     @PageableDefaults Pageable pageable,
	                     @RequestParam(required = false) String keywords) {

		pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
				defaultIfNull(pageable.getSort(),
						new Sort(Direction.ASC, "userName")));
		Page<User> pagedUser;
		if (StringUtils.isEmpty(keywords)) {
			pagedUser = userService.getPagedAll(role, pageable);
		} else {
			pagedUser = userService.getPagedAll(keywords, pageable);
			model.put("keywords", keywords);
		}
		model.addAttribute("users", pagedUser);
		EnumSet<Role> roleSet = EnumSet.allOf(Role.class);
		model.addAttribute("roleSet", roleSet);
		model.addAttribute("role", role);
		putPageIntoModelMap(model, pageable);
		return "user/list";
	}


	/**
	 * Get user creation form page.
	 *
	 * @param user  current user
	 * @param model mode
	 * @return "user/detail"
	 */
	@RequestMapping("/new")
	@PreAuthorize("hasAnyRole('A') or #user.userId == #userId")
	public String openForm(User user, final ModelMap model) {
		User one = User.createNew();
		model.addAttribute("user", one);
		model.addAttribute("allowUserIdChange", true);
		model.addAttribute("allowPasswordChange", true);
		model.addAttribute("allowRoleChange", false);
		model.addAttribute("newUser", true);
		model.addAttribute("roleSet", EnumSet.allOf(Role.class));
		model.addAttribute("showPasswordByDefault", true);
		attachCommonAttribute(one, model);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return "user/detail";
	}

	/**
	 * Get user detail page.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            current user
	 * @param model
	 *            mode
	 * @param userId
	 *            user to get
	 * @return "user/userDetail"
	 */
	@RequestMapping("/{userId}")
	@PreAuthorize("hasAnyRole('A') or #user.userId == #userId")
	public String getUserDetail(User user, final ModelMap model, @PathVariable final String userId) {
		model.addAttribute("roleSet", EnumSet.allOf(Role.class));
		User userFromDB = userService.getUserById(userId);
		model.addAttribute("user", userFromDB);
		model.addAttribute("userSecurity", config.isUserSecurityEnabled());
		getUserShareList(userFromDB, model);
=======
	 *
	 * @param model  mode
	 * @param userId user to get
	 * @return "user/detail"
	 */
	@RequestMapping("/{userId}")
	@PreAuthorize("hasAnyRole('A')")
	public String getOne(@PathVariable final String userId, ModelMap model) {
		User one = userService.getOne(userId);
		model.addAttribute("user", one);
		model.addAttribute("allowPasswordChange", true);
		model.addAttribute("allowRoleChange", true);
		model.addAttribute("roleSet", EnumSet.allOf(Role.class));
		model.addAttribute("showPasswordByDefault", false);
		attachCommonAttribute(one, model);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return "user/detail";
	}

	/**
<<<<<<< HEAD
	 * Save or Update user detail info.
	 * 
	 * @param user
	 *            current user
	 * @param model
	 *            model
	 * @param updatedUser
	 *            user to be updated.
	 * @param followersStr
	 *            user Id list that current will share his permission to.
	 * @return "redirect:/user/list" if current user change his info, otheriwise return "redirect:/"
	 */
	@RequestMapping("/save")
	@PreAuthorize("hasAnyRole('A') or #user.id == #updatedUser.id")
	public String saveOrUpdateUserDetail(User user, ModelMap model, @ModelAttribute("user") User updatedUser,
					@RequestParam(required = false) String followersStr) {
		checkArgument(updatedUser.validate());
		if (user.getRole() == Role.USER) {
			// General user can not change their role.
			User updatedUserInDb = userService.getUserById(updatedUser.getUserId());
=======
	 * Get the current user profile.
	 *
	 * @param user  current user
	 * @param model model
	 * @return "user/info"
	 */
	@RequestMapping("/profile")
	public String getOne(User user, ModelMap model) {
		checkNotEmpty(user.getUserId(), "UserID should not be NULL!");
		User one = userService.getOne(user.getUserId());
		model.addAttribute("user", one);
		model.addAttribute("allowPasswordChange", !config.isDemo());
		model.addAttribute("allowRoleChange", false);
		model.addAttribute("showPasswordByDefault", false);
		attachCommonAttribute(one, model);
		return "user/info";
	}

	/**
	 * Save or Update user detail info.
	 *
	 * @param user        current user
	 * @param model       model
	 * @param updatedUser user to be updated.
	 * @return "redirect:/user/list" if current user change his info, otherwise return "redirect:/"
	 */
	@RequestMapping("/save")
	@PreAuthorize("hasAnyRole('A') or #user.id == #updatedUser.id")
	public String save(User user, @ModelAttribute("user") User updatedUser, ModelMap model) {
		checkArgument(updatedUser.validate());
		if (user.getRole() == Role.USER) {
			// General user can not change their role.
			User updatedUserInDb = userService.getOne(updatedUser.getUserId());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			checkNotNull(updatedUserInDb);
			updatedUser.setRole(updatedUserInDb.getRole());

			// prevent user to modify with other user id
			checkArgument(updatedUserInDb.getId().equals(updatedUser.getId()), "Illegal request to update user:%s",
<<<<<<< HEAD
							updatedUser);
		}
		if (updatedUser.exist()) {
			userService.modifyUser(updatedUser, followersStr);
		} else {
			userService.saveUser(updatedUser);
		}
=======
					updatedUser);
		}
		save(updatedUser);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		model.clear();
		if (user.getId().equals(updatedUser.getId())) {
			return "redirect:/";
		} else {
			return "redirect:/user/";
		}
	}

<<<<<<< HEAD
	/**
	 * Delete users.
	 * 
	 * @param model
	 *            model
	 * @param userIds
	 *            comma separated user ids.
	 * @return "redirect:/user/list"
	 */
	@PreAuthorize("hasAnyRole('A')")
	@RequestMapping("/delete")
	public String deleteUser(ModelMap model, @RequestParam String userIds) {
		String[] ids = userIds.split(",");
		ArrayList<String> aListNumbers = new ArrayList<String>(Arrays.asList(ids));
		userService.deleteUsers(aListNumbers);
=======
	private User save(User user) {
		if (StringUtils.isBlank(user.getPassword())) {
			return userService.saveWithoutPasswordEncoding(user);
		} else {
			return userService.save(user);
		}
	}

	/**
	 * Delete users.
	 *
	 * @param model   model
	 * @param userIds comma separated user ids.
	 * @return "redirect:/user/"
	 */
	@PreAuthorize("hasAnyRole('A')")
	@RequestMapping("/delete")
	public String delete(User user, @RequestParam String userIds, ModelMap model) {
		String[] ids = userIds.split(",");
		for (String eachId : Arrays.asList(ids)) {
			if (!user.getUserId().equals(eachId)) {
				userService.delete(eachId);
			}
		}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		model.clear();
		return "redirect:/user/";
	}

<<<<<<< HEAD
	/**
	 * Check the user id existence.
	 * 
	 * @param model
	 *            model
	 * @param userId
	 *            userId to be checked
	 * @return success json if true.
	 */
	@PreAuthorize("hasAnyRole('A')")
	@RequestMapping("/{userId}/duplication_check")
	@ResponseBody
	public String checkUserId(ModelMap model, @PathVariable String userId) {
		User user = userService.getUserById(userId);
		return (user == null) ? returnSuccess() : returnError();
	}

	/**
	 * Get the current user profile.
	 * 
	 * @param user
	 *            current user
	 * @param model
	 *            model
	 * @return "user/userInfo"
	 */
	@RequestMapping("/profile")
	public String userProfile(User user, ModelMap model) {
		checkNotEmpty(user.getUserId(), "UserID should not be NULL!");
		User currentUser = userService.getUserById(user.getUserId());
		model.addAttribute("user", currentUser);
		model.addAttribute("demo", config.isDemo());
		getUserShareList(currentUser, model);
		model.addAttribute("action", "profile");
		model.addAttribute("userSecurity", config.isUserSecurityEnabled());
		return "user/info";
=======

	/**
	 * Get the follower list.
	 *
	 * @param user     current user
	 * @param keywords search keyword.
	 * @return json message
	 */
	@RestAPI
	@RequestMapping("/api/switch_options")
	public HttpEntity<String> switchOptions(User user,
	                                        @RequestParam(required = true) final String keywords) {
		return toJsonHttpEntity(getSwitchableUsers(user, keywords));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the follower list.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            current user
	 * @param model
	 *            model
	 * @return "user/switchOptions"
	 */
	@RequestMapping("/switch_options")
	public String switchOptions(User user, ModelMap model) {
		if (user.getRole().hasPermission(Permission.SWITCH_TO_ANYONE)) {
			List<User> allUserByRole = userService.getAllUserByRole(Role.USER.getFullName());
			model.addAttribute("shareUserList", allUserByRole);
		} else {
			User currUser = userService.getUserById(user.getUserId());
			model.addAttribute("shareUserList", currUser.getOwners());
		}
		return "user/switch_options";
	}

	/**
	 * Switch user identity.
	 * 
	 * @param user
	 *            current user
	 * @param model
	 *            model
	 * @param to
	 *            the user to whom a user will switch
	 * @param request
	 *            request
	 * @param response
	 *            response
	 * 
	 * @return redirect:/perftest/list
	 */
	@RequestMapping("/switch")
	public String switchUser(User user, ModelMap model, @RequestParam(required = false, defaultValue = "") String to,
					HttpServletRequest request, HttpServletResponse response) {
=======
	 *
	 * @param user  current user
	 * @param model model
	 * @return json message
	 */
	@RequestMapping("/switch_options")
	public String switchOptions(User user,
	                            ModelMap model) {
		model.addAttribute("switchableUsers", getSwitchableUsers(user, ""));
		return "user/switch_options";
	}


	private List<UserSearchResult> getSwitchableUsers(User user, String keywords) {
		List<UserSearchResult> result = newArrayList();
		if (user.getRole().hasPermission(Permission.SWITCH_TO_ANYONE)) {
			for (User each : userService.getPagedAll(keywords, new PageRequest(0, 10))) {
				result.add(new UserSearchResult(each));
			}
		} else {
			User currUser = userService.getOne(user.getUserId());
			for (User each : currUser.getOwners()) {
				result.add(new UserSearchResult(each));
			}
		}
		return result;
	}


	/**
	 * Switch user identity.
	 *
	 * @param model    model
	 * @param to       the user to whom a user will switch
	 * @param response response
	 * @return redirect:/perftest/
	 */
	@RequestMapping("/switch")
	public String switchUser(@RequestParam(required = false, defaultValue = "") String to,
	                         HttpServletRequest request, HttpServletResponse response, ModelMap model) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		Cookie cookie = new Cookie("switchUser", to);
		cookie.setPath("/");
		// Delete Cookie if empty switchUser
		if (StringUtils.isEmpty(to)) {
			cookie.setMaxAge(0);
		}
<<<<<<< HEAD
		response.addCookie(cookie);

		model.clear();
		return "redirect:/perftest/";
=======

		response.addCookie(cookie);
		model.clear();
		final String referer = request.getHeader("referer");
		return "redirect:" + StringUtils.defaultIfBlank(referer, "/");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get user list that current user will be shared, excluding current user.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            current user
	 * @param model
	 *            model
	 */
	private void getUserShareList(User user, ModelMap model) {
		if (user == null) {
			model.addAttribute("followers", Lists.newArrayList());
			model.addAttribute("shareUserList", Lists.newArrayList());
			return;
		}

		List<User> users = Lists.newArrayList();
		String userId = user.getUserId();
		for (User u : userService.getAllUserByRole(Role.USER.getFullName())) {
			if (u.getUserId().equals(userId)) {
				continue;
			}
			users.add(u.getUserBaseInfo());
		}
		model.addAttribute("followers", user.getFollowers());
		model.addAttribute("shareUserList", users);

=======
	 *
	 * @param user  current user
	 * @param model model
	 */
	protected void attachCommonAttribute(User user, ModelMap model) {
		List list = user.getFollowers() == null ? Lists.newArrayList() : user.getFollowers();
		model.addAttribute("followers", Lists.transform(list, new Function<User, UserSearchResult>() {
			public UserSearchResult apply(User user) {
				return new UserSearchResult(user);
			}
		}));
		model.addAttribute("allowShareChange", true);
		model.addAttribute("userSecurityEnabled", config.isUserSecurityEnabled());
	}

	/**
	 * Check if the given user id already exists.
	 *
	 * @param userId userId to be checked
	 * @return success json if true.
	 */
	@RestAPI
	@PreAuthorize("hasAnyRole('A')")
	@RequestMapping("/api/{userId}/check_duplication")
	public HttpEntity<String> checkDuplication(@PathVariable String userId) {
		User user = userService.getOne(userId);
		return (user == null) ? successJsonHttpEntity() : errorJsonHttpEntity();
	}

	/**
	 * Get users by the given role.
	 *
	 * @param role user role
	 * @return json message
	 */
	@RestAPI
	@PreAuthorize("hasAnyRole('A')")
	@RequestMapping(value = {"/api/", "/api"}, method = RequestMethod.GET)
	public HttpEntity<String> getAll(Role role) {
		return toJsonHttpEntity(userService.getAll(role));
	}

	/**
	 * Get the user by the given user id.
	 *
	 * @param userId user id
	 * @return json message
	 */
	@RestAPI
	@PreAuthorize("hasAnyRole('A')")
	@RequestMapping(value = "/api/{userId}", method = RequestMethod.GET)
	public HttpEntity<String> getOne(@PathVariable("userId") String userId) {
		return toJsonHttpEntity(userService.getOne(userId));
	}

	/**
	 * Create an user.
	 *
	 * @param newUser new user
	 * @return json message
	 */
	@RestAPI
	@PreAuthorize("hasAnyRole('A')")
	@RequestMapping(value = {"/api/", "/api"}, method = RequestMethod.POST)
	public HttpEntity<String> create(@ModelAttribute("user") User newUser) {
		checkNull(newUser.getId(), "User DB ID should be null");
		return toJsonHttpEntity(save(newUser));
	}

	/**
	 * Update the user.
	 *
	 * @param userId user id
	 * @param update update user
	 * @return json message
	 */
	@RestAPI
	@PreAuthorize("hasAnyRole('A')")
	@RequestMapping(value = "/api/{userId}", method = RequestMethod.PUT)
	public HttpEntity<String> update(@PathVariable("userId") String userId, User update) {
		update.setUserId(userId);
		checkNull(update.getId(), "User DB ID should be null");
		return toJsonHttpEntity(save(update));
	}

	/**
	 * Delete the user by the given userId.
	 *
	 * @param userId user id
	 * @return json message
	 */
	@RestAPI
	@PreAuthorize("hasAnyRole('A')")
	@RequestMapping(value = "/api/{userId}", method = RequestMethod.DELETE)
	public HttpEntity<String> delete(User user, @PathVariable("userId") String userId) {
		if (!user.getUserId().equals(userId)) {
			userService.delete(userId);
		}
		return successJsonHttpEntity();
	}

	/**
	 * Search user list on the given keyword.
	 *
	 * @param pageable page info
	 * @param keywords search keyword.
	 * @return json message
	 */
	@RestAPI
	@RequestMapping(value = "/api/search", method = RequestMethod.GET)
	public HttpEntity<String> search(User user, @PageableDefaults Pageable pageable,
	                                 @RequestParam(required = true) String keywords) {
		pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
				defaultIfNull(pageable.getSort(),
						new Sort(Direction.ASC, "userName")));
		Page<User> pagedUser = userService.getPagedAll(keywords, pageable);
		List<UserSearchResult> result = newArrayList();
		for (User each : pagedUser) {
			result.add(new UserSearchResult(each));
		}

		final String currentUserId = user.getUserId();
		CollectionUtils.filter(result, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				UserSearchResult each = (UserSearchResult) object;
				return !(each.getId().equals(currentUserId) || each.getId().equals(ControllerConstants.NGRINDER_INITIAL_ADMIN_USERID));
			}
		});

		return toJsonHttpEntity(result);
	}

	public static class UserSearchResult {
		@Expose
		final private String id;

		@Expose
		final private String text;

		public UserSearchResult(User user) {
			id = user.getUserId();
			final String email = user.getEmail();
			final String userName = user.getUserName();
			if (StringUtils.isEmpty(email)) {
				this.text = userName + " (" + id + ")";
			} else {
				this.text = userName + " (" + email + " / " + id + ")";
			}
		}

		public String getText() {
			return text;
		}

		public String getId() {
			return id;
		}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}
}
