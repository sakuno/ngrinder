/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.user.service;

<<<<<<< HEAD
import static org.ngrinder.common.util.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
=======
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.ngrinder.common.constant.ControllerConstants;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.infra.config.Config;
import org.ngrinder.model.PerfTest;
import org.ngrinder.model.Role;
import org.ngrinder.model.User;
import org.ngrinder.perftest.service.PerfTestService;
import org.ngrinder.script.service.FileEntryService;
import org.ngrinder.security.SecuredUser;
<<<<<<< HEAD
import org.ngrinder.service.IUserService;
import org.ngrinder.user.repository.UserRepository;
import org.ngrinder.user.repository.UserSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
=======
import org.ngrinder.service.AbstractUserService;
import org.ngrinder.user.repository.UserRepository;
import org.ngrinder.user.repository.UserSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

<<<<<<< HEAD
/**
 * The Class UserService.
 * 
=======
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The Class UserService.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author Yubin Mao
 * @author AlexQin
 */
@Service
<<<<<<< HEAD
public class UserService implements IUserService {
=======
public class UserService extends AbstractUserService {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	@Autowired
	private UserRepository userRepository;

<<<<<<< HEAD
=======
	@SuppressWarnings("SpringJavaAutowiringInspection")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Autowired
	private PerfTestService perfTestService;

	@Autowired
	private FileEntryService scriptService;

	@Autowired
	private SaltSource saltSource;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private Config config;

<<<<<<< HEAD
	/**
	 * get user by user id.
	 * 
	 * @param userId
	 *            user id
	 * @return user
	 */
	@Transactional
	@Cacheable("users")
	public User getUserById(String userId) {
		return userRepository.findOneByUserId(userId);
	}

	/**
	 * get user by user id without using Cache. The user in cache has no followers and owners
	 * initialized.
	 * 
	 * @param userId
	 *            user id
	 * @return user
	 */
	@Transactional
	public User getUserByIdWithoutCache(String userId) {
		User user = userRepository.findOneByUserId(userId);
		if (user != null) {
			Hibernate.initialize(user.getOwners());
			Hibernate.initialize(user.getFollowers());
		}
		return user;
=======
	@Autowired
	private CacheManager cacheManager;

	private Cache userCache;

	@PostConstruct
	public void init() {
		userCache = cacheManager.getCache("users");
	}

	/**
	 * Get user by user id.
	 *
	 * @param userId user id
	 * @return user
	 */
	@Transactional
	@Cacheable("users")
	@Override
	public User getOne(String userId) {
		return userRepository.findOneByUserId(userId);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Encoding given user's password.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            user
=======
	 *
	 * @param user user
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void encodePassword(User user) {
		if (StringUtils.isNotBlank(user.getPassword())) {
			SecuredUser securedUser = new SecuredUser(user, null);
			String encodePassword = passwordEncoder.encodePassword(user.getPassword(), saltSource.getSalt(securedUser));
			user.setPassword(encodePassword);
		}
	}

<<<<<<< HEAD
	/**
	 * Get all user by the given role.
	 * 
	 * @param roleName
	 *            role name
	 * @param pageable
	 *            pageable
	 * @return page of user
	 */
	public Page<User> getAllUserByRole(String roleName, Pageable pageable) {
		if (StringUtils.isBlank(roleName)) {
			return userRepository.findAll(pageable);
		} else {
			return getUserListByRole(getRole(roleName), pageable);
		}
	}

	/**
	 * get all users by role.
	 * 
	 * @param roleName
	 *            role name
	 * @return found user list
	 */
	public List<User> getAllUserByRole(String roleName) {
		return getAllUserByRole(roleName, new Sort(Direction.ASC, "userName"));
	}

	/**
	 * get all users by role.
	 * 
	 * @param roleName
	 *            role name
	 * @param sort
	 *            sort method
	 * @return found user list
	 */
	public List<User> getAllUserByRole(String roleName, Sort sort) {
		if (StringUtils.isBlank(roleName)) {
			return userRepository.findAll(sort);
		} else {
			return getUserListByRole(getRole(roleName), sort);
		}
	}

	/**
	 * create user.
	 * 
	 * @param user
	 *            include id, userID, fullName, role, password.
	 * 
	 * @return result
	 */
	@Transactional
	@CacheEvict(value = "users", key = "#user.userId")
	public User saveUser(User user) {
		encodePassword(user);
		return saveUserWithoutPasswordEncoding(user);
	}

	/**
	 * create user.
	 * 
	 * @param user
	 *            include id, userID, fullName, role, password.
	 * 
	 * @return result
	 */
	@Transactional
	@CacheEvict(value = "users", key = "#user.userId")
	public User saveUserWithoutPasswordEncoding(User user) {
		User createdUser = userRepository.save(user);
		prepareUserEnv(user);
		return createdUser;
	}
	
=======

	/**
	 * Save user.
	 *
	 * @param user include id, userID, fullName, role, password.
	 * @return User
	 */
	@Transactional
	@CachePut(value = "users", key = "#user.userId")
	@Override
	public User save(User user) {
		encodePassword(user);
		return saveWithoutPasswordEncoding(user);
	}

	/**
	 * Save user.
	 *
	 * @param user include id, userID, fullName, role, password.
	 * @return User
	 */
	@Transactional
	@CachePut(value = "users", key = "#user.userId")
	@Override
	public User saveWithoutPasswordEncoding(User user) {
		final List<User> followers = getFollowers(user.getFollowersStr());
		user.setFollowers(followers);
		if (user.getPassword() != null && StringUtils.isBlank(user.getPassword())) {
			user.setPassword(null);
		}
		final User existing = userRepository.findOneByUserId(user.getUserId());
		if (existing != null) {
			// First expire existing followers.
			final List<User> existingFollowers = existing.getFollowers();
			if (existingFollowers != null) {
				for (User eachFollower : existingFollowers) {
					userCache.evict(eachFollower.getUserId());
				}
			}
			user = existing.merge(user);
		}
		User createdUser = userRepository.save(user);
		// Then expires new followers so that new followers info can be loaded.
		for (User eachFollower : followers) {
			userCache.evict(eachFollower.getUserId());
		}
		prepareUserEnv(createdUser);
		return createdUser;
	}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private void prepareUserEnv(User user) {
		scriptService.prepare(user);
	}

<<<<<<< HEAD
	/**
	 * Add user.
	 * 
	 * @param user
	 *            user
	 * @param role
	 *            role
	 */
	@CacheEvict(value = "users", key = "#user.userId")
	public void saveUser(User user, Role role) {
		encodePassword(user);
		user.setRole(role);
		userRepository.save(user);
	}

	/**
	 * modify user information.
	 * 
	 * @param user
	 *            user
	 * @param shareUserIds
	 *            It is a list of user IDs to share the permission of user
	 * @return user id
	 */
	@Transactional
	@CacheEvict(value = "users", key = "#user.userId")
	public String modifyUser(User user, String shareUserIds) {
		checkNotNull(user, "user should be not null, when modifying user");
		checkNotNull(user.getId(), "user id should be provided when modifying user");

		shareUserIds = (String) ObjectUtils.defaultIfNull(shareUserIds, "");
		List<User> newShareUsers = new ArrayList<User>();
		String[] userIds = shareUserIds.split(",");
		for (String userId : userIds) {
			User shareUser = userRepository.findOneByUserId(userId.trim());
			newShareUsers.add(shareUser);
		}
		user.setFollowers(newShareUsers);

		encodePassword(user);
		User targetUser = userRepository.findOne(user.getId());
		targetUser.merge(user);
		userRepository.save(targetUser);
		return user.getUserId();
	}

	/**
	 * Delete user. All corresponding perftest and directories are deleted as well.
	 * 
	 * @param userIds
	 *            the user id string list
	 */
	@Transactional
	@CacheEvict(value = "users", allEntries = true)
	public void deleteUsers(List<String> userIds) {
		for (String userId : userIds) {
			User user = getUserById(userId);
			List<PerfTest> deletePerfTests = perfTestService.deleteAllPerfTests(user);
			userRepository.delete(user);
			for (PerfTest perfTest : deletePerfTests) {
				FileUtils.deleteQuietly(config.getHome().getPerfTestDirectory(perfTest));
			}
			FileUtils.deleteQuietly(config.getHome().getScriptDirectory(user));
			FileUtils.deleteQuietly(config.getHome().getUserRepoDirectory(user));
		}
	}

	/**
	 * get the user list by the given role.
	 * 
	 * @param role
	 *            role
	 * @param sort
	 *            sort
	 * @return found user list
	 * @throws Exception
	 */
	public List<User> getUserListByRole(Role role, Sort sort) {
		return userRepository.findAllByRole(role, sort);
=======

	private List<User> getFollowers(String followersStr) {
		List<User> newShareUsers = new ArrayList<User>();
		String[] userIds = StringUtils.split(StringUtils.trimToEmpty(followersStr), ',');
		for (String userId : userIds) {
			User shareUser = userRepository.findOneByUserId(userId.trim());
			if (shareUser != null) {
				newShareUsers.add(shareUser);
			}
		}
		return newShareUsers;
	}

	/**
	 * Delete the given user. All corresponding perftest and directories are deleted as well.
	 *
	 * @param userId the user id string list
	 */
	@SuppressWarnings("SpringElInspection")
	@Transactional
	@CacheEvict(value = "users", key = "#userId")
	public void delete(String userId) {
		User user = getOne(userId);
		List<PerfTest> deletePerfTests = perfTestService.deleteAll(user);
		userRepository.delete(user);
		for (PerfTest perfTest : deletePerfTests) {
			FileUtils.deleteQuietly(config.getHome().getPerfTestDirectory(perfTest));
		}
		FileUtils.deleteQuietly(config.getHome().getScriptDirectory(user));
		FileUtils.deleteQuietly(config.getHome().getUserRepoDirectory(user));
	}

	/**
	 * Get the user list by the given role.
	 *
	 * @param role role
	 * @param sort sort
	 * @return found user list
	 */
	public List<User> getAll(Role role, Sort sort) {
		return (role == null) ? userRepository.findAll(sort) : userRepository.findAllByRole(role, sort);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * get the user list by the given role.
<<<<<<< HEAD
	 * 
	 * @param role
	 *            role
	 * @param pageable
	 *            sort
	 * @return found user list
	 * @throws Exception
	 */
	public Page<User> getUserListByRole(Role role, Pageable pageable) {
		return userRepository.findAllByRole(role, pageable);
	}

	/**
	 * get the user list by the given role.
	 * 
	 * @param role
	 *            role
	 * @return found user list
	 * @throws Exception
	 */
	public List<User> getUserListByRole(Role role) {
		return getUserListByRole(role, new Sort(Direction.ASC, "userName"));
	}

	/**
	 * get Role object based on role name.
	 * 
	 * @param roleName
	 *            role name
	 * @return found Role
	 */
	public Role getRole(String roleName) {
		if (Role.ADMIN.getFullName().equals(roleName)) {
			return Role.ADMIN;
		} else if (Role.USER.getFullName().equals(roleName)) {
			return Role.USER;
		} else if (Role.SUPER_USER.getFullName().equals(roleName)) {
			return Role.SUPER_USER;
		} else if (Role.SYSTEM_USER.getFullName().equals(roleName)) {
			return Role.SYSTEM_USER;
		} else {
			return null;
		}
	}

	/**
	 * Get the user list by nameLike spec.
	 * 
	 * @param name
	 *            name of user
	 * @return found user list
	 */
	public List<User> getUserListByKeyWord(String name) {
=======
	 *
	 * @param role     role
	 * @param pageable sort
	 * @return found user list
	 */
	public Page<User> getPagedAll(Role role, Pageable pageable) {
		return (role == null) ? userRepository.findAll(pageable) : userRepository.findAllByRole(role, pageable);
	}

	/**
	 * Get the users by the given role.
	 *
	 * @param role role
	 * @return found user list
	 */
	public List<User> getAll(Role role) {
		return getAll(role, new Sort(Direction.ASC, "userName"));
	}

	/**
	 * Get the users by nameLike spec.
	 *
	 * @param name name of user
	 * @return found user list
	 */
	public List<User> getAll(String name) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return userRepository.findAll(UserSpecification.nameLike(name));
	}

	/**
	 * Get user page by the given keyword.
<<<<<<< HEAD
	 * 
	 * @param namelike
	 *            keyword to be like search.
	 * @param pageable
	 *            page
	 * @return user page
	 */
	public Page<User> getUserListByKeyWord(String namelike, Pageable pageable) {
		return userRepository.findAll(UserSpecification.nameLike(namelike), pageable);
	}

=======
	 *
	 * @param keyword  keyword to be like search.
	 * @param pageable page
	 * @return user page
	 */
	public Page<User> getPagedAll(String keyword, Pageable pageable) {
		return userRepository.findAll(UserSpecification.nameLike(keyword), pageable);
	}

	/**
	 * Create an user avoiding ModelAspect behavior.
	 *
	 * @param user userID, fullName, role, password.
	 * @return User
	 */
	@Transactional
	@CachePut(value = "users", key = "#user.userId")
	@Override
	public User createUser(User user) {
		encodePassword(user);
		Date createdDate = new Date();
		user.setCreatedDate(createdDate);
		user.setLastModifiedDate(createdDate);
		User createdUser = getOne(ControllerConstants.NGRINDER_INITIAL_ADMIN_USERID);
		user.setCreatedUser(createdUser);
		user.setLastModifiedUser(createdUser);
		return saveWithoutPasswordEncoding(user);
	}


>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
}
