<!DOCTYPE html>
<html>
<head>
	<#include "../common/common.ftl"> 
	<#include "../common/datatables.ftl">
	<title><@spring.message "user.list.title"/></title>
</head>

<body>
<<<<<<< HEAD
=======
<div id="wrap">
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	<#include "../common/navigator.ftl">
	<div class="container">
		<fieldSet>
			<legend class="header">
<<<<<<< HEAD
				<@spring.message "navigator.dropdown.userManagement"/>
				<select id="roles" class="pull-right" name="roles">
					<option value="all" <#if listPage?exists && !roleName?exists>selected</#if>"><@spring.message "user.left.all"/></option>
					<#list roleSet as role> 
						<option value="${role.fullName}" <#if roleName?exists && role.fullName == roleName>selected</#if>>${role.fullName}</option>
=======
				<@spring.message "navigator.dropDown.userManagement"/>
				<select id="roles" class="pull-right" name="roles">
					<option value="all" <#if listPage?? && !role??>selected</#if> >
						<@spring.message "user.left.all"/>
					</option>
					<#list roleSet as each>
						<option value="${each}" <#if role?? && role == each>selected</#if>>${each.fullName}</option>
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					</#list>
				</select>
			</legend> 
		</fieldSet>
<<<<<<< HEAD
=======
		<!--suppress HtmlUnknownTarget -->
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		<form id="user_list_form" action="${req.getContextPath()}/user" method="POST">
			<div class="well form-inline search-bar">			 
				<input type="text" class="search-query search-query-without-radios" placeholder="Keywords" id="search_text" name="keywords" value="${keywords!}">
				<a class="btn" id="search_user">
					<i class="icon-search"></i> <@spring.message "common.button.search"/>
				</a>
				<span class="pull-right">
<<<<<<< HEAD
					<a class="btn" href="${req.getContextPath()}/user/new" id="createBtn" data-toggle="modal">
=======
					<a class="btn" href="${req.getContextPath()}/user/new">
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
						<i class="icon-user"></i> <@spring.message "user.list.button.create"/>
					</a>
					<a href="javascript:deleteCheckedUsers()" class="btn btn-danger">
						<i class="icon-remove icon-white"></i> <@spring.message "user.list.button.delete"/>
					</a>
				</span>
			</div>
			<input type="hidden" id="page_number" name="page.page" value="${page.pageNumber + 1}"/>
			<input type="hidden" id="page_size" name="page.size" value="${page.pageSize}"/>
			<input type="hidden" id="sort_column" name="page.sort" value="${sortColumn!'lastModifiedDate'}">
			<input type="hidden" id="sort_direction" name="page.sort.dir" value="${sortDirection!'desc'}">
<<<<<<< HEAD
		
		</form>
		<table class="table table-striped table-bordered ellipsis" id="user_table">
			<#assign userList = userPage.content/>
=======

		</form>
		<table class="table table-striped table-bordered ellipsis" id="user_table">
			<#assign userList = users.content/>
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			<colgroup>
				<col width="30">
				<col width="120">
				<col width="120">
				<col width="160"> 
				<col>
				<col width="120">
				<col width="45">
				<col width="45">
			</colgroup>
			<thead>
				<tr>
					<th class="no-click nothing"><input type="checkbox" class="checkbox" value=""></th>
<<<<<<< HEAD
					<th name="userName"><@spring.message "user.option.name"/></th>
					<th class="no-click nothing"><@spring.message "user.option.role"/></th>
					<th name="email"><@spring.message "user.option.email"/></th>
=======
					<th name="userName"><@spring.message "user.info.name"/></th>
					<th class="no-click nothing"><@spring.message "user.info.role"/></th>
					<th name="email"><@spring.message "user.info.email"/></th>
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					<th class="no-click nothing"><@spring.message "common.label.description"/></th>
					<th name="createdDate"><@spring.message "user.list.table.date"/></th>
					<th class="no-click nothing"><@spring.message "user.list.table.edit"/></th>
					<th class="no-click nothing"><@spring.message "user.list.table.delete"/></th>
				</tr>
			</thead>
			<tbody>
<<<<<<< HEAD
				
				<#list userList as user>
				<tr class='${["odd", ""][user_index%2]}'>
					<td class="center"><input type="checkbox" class="checkbox" id="user_info_check"<#if user.userId == "admin">disabled</#if>
						value="${user.userId}" uname="${user.userName}"/></td>
=======

				<@list list_items=userList others="table_list" colspan="8"; user>
				<tr>
					<td class="center">
						<input type="checkbox" class="checkbox" id="user_info_check"
							<#if user.userId == "admin">disabled</#if>
							value="${user.userId}" uname="${user.userName}"/>
					</td>
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					<td class="ellipsis"><a href="${req.getContextPath()}/user/${user.userId}">${user.userName}</a></td>
					<td title="${user.role.fullName}">${user.role.fullName}</td>
					<td class="ellipsis">${user.email!""}</td>
					<td class="ellipsis">${user.description!}</td>
					<td><#if user.createdDate?has_content> ${user.createdDate?string("yyyy-MM-dd HH:mm")} </#if></td>
					<td class="center">
						<a href="${req.getContextPath()}/user/${user.userId}">
							<i class="icon-edit"></i>
						</a>
					</td>
					<td class="center">
						<#if user.userId != "admin">
						<a href="javascript:deleteUsers('${user.userId}', '${user.userName}');">
							<i class="icon-remove"></i>
						</a>
						</#if>
					</td>
				</tr>
<<<<<<< HEAD
				</#list>
=======
				</@list>
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			</tbody>
		</table>
		<#if userList?has_content>
			<#include "../common/paging.ftl">
<<<<<<< HEAD
			<@paging  userPage.totalElements userPage.number+1 userPage.size 10 ""/>
=======
			<@paging  users.totalElements users.number+1 users.size 10 ""/>
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			<script type="text/javascript">
				function doSubmit(page) {
					getList(page);
				}
			</script>
		</#if>
<<<<<<< HEAD
		<#include "../common/copyright.ftl">
	</div>
=======
	</div>
</div>
<#include "../common/copyright.ftl">
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	<script type="text/javascript">
		function getList(page) {
			$("#page_number").val(page);
			document.forms.user_list_form.submit();
		}
<<<<<<< HEAD
		
=======

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		$(document).ready(function(){
			$("#search_user").click(function() {
				$("#user_list_form").submit();
			});
<<<<<<< HEAD
			
=======

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			$("#roles").change(function() {
				var selectedValue = $(this).val();
				var destUrl = "${req.getContextPath()}/user/";
				if (selectedValue != "all") {
<<<<<<< HEAD
					destUrl = destUrl + "?roleName=" + selectedValue;
=======
					destUrl = destUrl + "?role=" + selectedValue;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				}
				window.location.href=destUrl;
			});
			removeClick();
<<<<<<< HEAD
			enableChkboxSelectAll("user_table");
=======
			enableCheckboxSelectAll("user_table");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			$("th").each(function() {
				var $this = $(this);
				if (!$this.hasClass("nothing")) {
					$this.addClass("sorting");
				}
			});
			var sortColumn = $("#sort_column").val();
			var sortDir = $("#sort_direction").val().toLowerCase();
<<<<<<< HEAD
			
=======

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			$("th[name='" + sortColumn + "']").addClass("sorting_" + sortDir);

			$("th.sorting").click(function() {
				var $currObj = $(this);
				var sortDirection = "ASC";
				if ($currObj.hasClass("sorting_asc")) {
					sortDirection = "DESC";
				}
<<<<<<< HEAD
				
=======

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				$("#sort_column").val($currObj.attr('name'));
				$("#sort_direction").val(sortDirection);
				getList(1);
			});

		});
<<<<<<< HEAD
	
=======

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		function deleteCheckedUsers() {
			var list = $("input[id='user_info_check']:checked");
			if(list.length == 0) {
				bootbox.alert('<@spring.message "user.list.alert.delete"/>', '<@spring.message "common.button.ok"/>');
				return;
			}
			var checkedUserName = [];
			var checkedUserId = [];
<<<<<<< HEAD
			var $elem;
			list.each(function() {
				$elem = $(this);
				checkedUserName.push($elem.attr("uname"));
				checkedUserId.push($elem.val());
			});
			
			deleteUsers(checkedUserId.join(","), checkedUserName.join(", "));	
		}
		
=======
			list.each(function() {
				var $elem = $(this);
				checkedUserName.push($elem.attr("uname"));
				checkedUserId.push($elem.val());
			});

			deleteUsers(checkedUserId.join(","), checkedUserName.join(", "));	
		}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		function deleteUsers(ids, names) {
			bootbox.confirm('<@spring.message "user.list.confirm.delete"/> ' + names + '?', '<@spring.message "common.button.cancel"/>', '<@spring.message "common.button.ok"/>', function(result) {
				if (result) {
					document.location.href="${req.getContextPath()}/user/delete?userIds=" + ids;
				}
			});
		}
	</script>
</body>
</html>
