/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.agent.service;

<<<<<<< HEAD
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Map;

import junit.framework.Assert;
import net.grinder.message.console.AgentControllerState;

=======
import junit.framework.Assert;
import net.grinder.message.console.AgentControllerState;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.apache.commons.lang.mutable.MutableInt;
import org.junit.Test;
import org.ngrinder.AbstractNGrinderTransactionalTest;
import org.ngrinder.agent.repository.AgentManagerRepository;
import org.ngrinder.infra.config.Config;
import org.ngrinder.model.AgentInfo;
import org.springframework.beans.factory.annotation.Autowired;

<<<<<<< HEAD
/**
 * Agent service test.
 * 
=======
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.ngrinder.common.util.TypeConvertUtils.cast;

/**
 * Agent service test.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author Tobi
 * @since 3.0
 */
public class AgentManagerServiceTest extends AbstractNGrinderTransactionalTest {

	@Autowired
	private AgentManagerService agentManagerService;

	@Autowired
<<<<<<< HEAD
	private AgentManagerRepository agentRepository;

	@Autowired
=======
	private AgentPackageService agentPackageService;

	@Autowired
	private AgentManagerRepository agentRepository;

	@Autowired
	private LocalAgentService localAgentService;

	@Autowired
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private Config config;

	@Test
	public void testSaveGetDeleteAgent() {
		AgentInfo agent = saveAgent("save");
<<<<<<< HEAD
		AgentInfo agent2 = agentManagerService.getAgent(agent.getId(), false);
		Assert.assertNotNull(agent2);

		List<AgentInfo> agentListDB = agentManagerService.getLocalAgentListFromDB();
		agentListDB = agentManagerService.getLocalAgentListFromDB();
=======
		AgentInfo agent2 = agentManagerService.getOne(agent.getId());
		Assert.assertNotNull(agent2);

		List<AgentInfo> agentListDB = agentManagerService.getAllLocal();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		Assert.assertNotNull(agentListDB);

		agentManagerService.approve(agent.getId(), true);

<<<<<<< HEAD
		agentManagerService.deleteAgent(agent.getId());
		agent2 = agentManagerService.getAgent(agent.getId(), false);
=======
		agentRepository.delete(agent.getId());
		agent2 = agentManagerService.getOne(agent.getId());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		Assert.assertNull(agent2);
	}

	private AgentInfo saveAgent(String key) {
		AgentInfo agent = new AgentInfo();
		agent.setIp("1.1.1.1");
		agent.setName("testAppName" + key);
		agent.setPort(8080);
		agent.setRegion("testRegion" + key);
<<<<<<< HEAD
		agent.setStatus(AgentControllerState.BUSY);
		agentManagerService.saveAgent(agent);
=======
		agent.setState(AgentControllerState.BUSY);
		agentRepository.save(agent);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return agent;
	}

	@Test
	public void testGetUserAvailableAgentCount() {
<<<<<<< HEAD
		Map<String, MutableInt> countMap = agentManagerService.getUserAvailableAgentCountMap(getTestUser());
=======
		Map<String, MutableInt> countMap = agentManagerService.getAvailableAgentCountMap(getTestUser());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		String currRegion = config.getRegion();
		int oriCount = countMap.get(currRegion).intValue();

		AgentInfo agentInfo = new AgentInfo();
		agentInfo.setName("localhost");
		agentInfo.setRegion(config.getRegion());
		agentInfo.setIp("127.127.127.127");
		agentInfo.setPort(1);
<<<<<<< HEAD
		agentInfo.setStatus(AgentControllerState.READY);
		agentInfo.setApproved(true);
		agentManagerService.saveAgent(agentInfo);
		countMap = agentManagerService.getUserAvailableAgentCountMap(getTestUser());
=======
		agentInfo.setState(AgentControllerState.READY);
		agentInfo.setApproved(true);
		agentRepository.save(agentInfo);
		localAgentService.expireCache();
		countMap = agentManagerService.getAvailableAgentCountMap(getTestUser());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

		int newCount = countMap.get(config.getRegion()).intValue();
		assertThat(newCount, is(oriCount + 1));
	}

	@Test
<<<<<<< HEAD
	public void testCheckAgentStatus() {
=======
	public void testCheckAgentState() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		AgentInfo agentInfo = new AgentInfo();
		agentInfo.setName("localhost");
		agentInfo.setRegion(config.getRegion());
		agentInfo.setIp("127.127.127.127");
		agentInfo.setPort(1);
<<<<<<< HEAD
		agentInfo.setStatus(AgentControllerState.READY);
		agentManagerService.saveAgent(agentInfo);
		agentManagerService.checkAgentStatusRegularly();
=======
		agentInfo.setState(AgentControllerState.READY);
		agentRepository.save(agentInfo);
		localAgentService.expireCache();
		agentManagerService.checkAgentState();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

		AgentInfo agentInDB = agentRepository.findOne(agentInfo.getId());
		assertThat(agentInDB.getIp(), is(agentInfo.getIp()));
		assertThat(agentInDB.getName(), is(agentInfo.getName()));
<<<<<<< HEAD
		assertThat(agentInDB.getStatus(), is(AgentControllerState.INACTIVE));
=======
		assertThat(agentInDB.getState(), is(AgentControllerState.INACTIVE));
	}

	@Test
	public void testCompressAgentFolder() throws IOException, URISyntaxException {
		URLClassLoader loader = (URLClassLoader) this.getClass().getClassLoader();
		URL core = this.getClass().getClassLoader().getResource("lib/ngrinder-core-test.jar");
		URL sh = this.getClass().getClassLoader().getResource("lib/ngrinder-sh-test.jar");
		URL[] ls = {core, sh};
		URL[] urls = loader.getURLs();
		URL[] allLib = cast(ArrayUtils.addAll(urls, ls));
		URLClassLoader child = new URLClassLoader(allLib, this.getClass().getClassLoader());
		File agentUpgrade = agentPackageService.createAgentPackage(child, null, null, 10000, null);
		FileUtils.deleteQuietly(agentUpgrade);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	@Test
	public void testOther() {
<<<<<<< HEAD
		agentManagerService.getLocalAgents();
		agentManagerService.getLocalAgentIdentityByIpAndName("127.0.0.1", "127.0.0.1");
		agentManagerService.stopAgent(0L);
		agentManagerService.requestShareAgentSystemDataModel(0L);
		agentManagerService.getAgentSystemDataModel("127.0.0.1", "127.0.0.1");
		agentManagerService.setAgentManager(agentManagerService.getAgentManager());
		agentManagerService.setAgentRepository(agentManagerService.getAgentRepository());
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

}
