/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.agent.controller;

<<<<<<< HEAD
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import net.grinder.message.console.AgentControllerState;

=======
import net.grinder.message.console.AgentControllerState;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ngrinder.AbstractNGrinderTransactionalTest;
<<<<<<< HEAD
=======
import org.ngrinder.agent.repository.AgentManagerRepository;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.agent.service.AgentManagerService;
import org.ngrinder.infra.config.Config;
import org.ngrinder.model.AgentInfo;
import org.springframework.beans.factory.annotation.Autowired;
<<<<<<< HEAD
=======
import org.springframework.http.HttpEntity;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

<<<<<<< HEAD
/**
 * Class description.
 * 
 * @author Mavlarn
 * @since 3.0
 */
=======
import java.io.File;
import java.io.IOException;
import java.util.Collection;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
public class AgentManagerControllerTest extends AbstractNGrinderTransactionalTest {

	@Autowired
	AgentManagerController agentController;

	@Autowired
<<<<<<< HEAD
=======
	AgentManagerRepository agentManagerRepository;

	@Autowired
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	AgentManagerService agentService;

	@Autowired
	private Config config;

	@Before
	public void setMockRequest() {
		MockHttpServletRequest req = new MockHttpServletRequest();
		req.addHeader("User-Agent", "Win");
		SecurityContextHolderAwareRequestWrapper reqWrapper = new SecurityContextHolderAwareRequestWrapper(req, "U");
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(reqWrapper));
	}

	@After
	public void reSetRequest() {
		RequestContextHolder.resetRequestAttributes();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetAgentList() {

		ModelMap model = new ModelMap();
<<<<<<< HEAD
		agentController.getAgentList("", model);
=======
		agentController.getAll("", model);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

		// create a temp download dir and file for this function
		File directory = config.getHome().getDownloadDirectory();
		if (!directory.exists()) {
			try {
				FileUtils.forceMkdir(directory);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		File tmpDownFile;
		try {
			tmpDownFile = File.createTempFile("ngrinder", "zip", directory);
			FileUtils.writeStringToFile(tmpDownFile, "test data");
			tmpDownFile.deleteOnExit();
		} catch (IOException e) {
			e.printStackTrace();
		}

		model.clear();
<<<<<<< HEAD
		agentController.getAgentList("", model);
		Collection<AgentInfo> agents = (Collection<AgentInfo>) model.get("agents");
		if (!agents.isEmpty()) {
			AgentInfo testAgt = agents.iterator().next();
			model.clear();
			agentController.getAgent(testAgt.getId(), model);
			AgentInfo agentInDB = (AgentInfo) model.get("agent");
			assertThat(agentInDB.getId(), is(testAgt.getId()));
			assertThat(agentInDB.getIp(), is(testAgt.getIp()));
			assertThat(agentInDB.getPort(), is(testAgt.getPort()));

		}
=======
		agentController.getAll("", model);
		Collection<AgentInfo> agents = (Collection<AgentInfo>) model.get("agents");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	@Test
	public void testApproveAgent() {
		AgentInfo agent = new AgentInfo();
		agent.setApproved(false);
		agent.setName("Test-Host");
		agent.setIp("127.0.0.1");
<<<<<<< HEAD
		agent.setStatus(AgentControllerState.READY);
		agentService.saveAgent(agent);

		ModelMap model = new ModelMap();
		// test get agent
		agentController.getAgent(agent.getId(), model);
		AgentInfo agentinDB = (AgentInfo) model.get("agent");
		assertThat(agentinDB.getName(), is(agent.getName()));
		assertThat(agentinDB.getIp(), is(agent.getIp()));
		assertThat(agentinDB.isApproved(), is(false));

		// test approve agent
		model.clear();
		agentController.approveAgent(agentinDB.getId(), true, "", model);
		agentController.getAgent(agent.getId(), model);
		agentinDB = (AgentInfo) model.get("agent");
		assertThat(agentinDB.isApproved(), is(true));

		// test un-approve
		model.clear();
		agentController.approveAgent(agentinDB.getId(), false, "", model);
		agentController.getAgent(agent.getId(), model);
		agentinDB = (AgentInfo) model.get("agent");
		assertThat(agentinDB.isApproved(), is(false));
=======
		agent.setState(AgentControllerState.READY);
		agentManagerRepository.save(agent);

		ModelMap model = new ModelMap();
		// test get agent
		agentController.getOne(agent.getId(), model);
		AgentInfo agentInDB = (AgentInfo) model.get("agent");
		assertThat(agentInDB.getName(), is(agent.getName()));
		assertThat(agentInDB.getIp(), is(agent.getIp()));
		assertThat(agentInDB.isApproved(), is(false));

		// test approve agent
		model.clear();
		agentController.approve(agentInDB.getId());
		agentController.getOne(agent.getId(), model);
		agentInDB = (AgentInfo) model.get("agent");
		assertThat(agentInDB.isApproved(), is(true));

		// test un-approve
		model.clear();
		agentController.disapprove(agentInDB.getId());
		agentController.getOne(agent.getId(), model);
		agentInDB = (AgentInfo) model.get("agent");
		assertThat(agentInDB.isApproved(), is(false));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	@Test
	public void testStopAgent() {
<<<<<<< HEAD
		agentController.stopAgent("0");
=======
		agentController.stop("0");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	@Test
	public void testGetCurrentMonitorData() {
<<<<<<< HEAD
		ModelMap model = new ModelMap();
		String rtnStr = agentController.getCurrentMonitorData(0L, "127.0.0.1", "127.0.0.1", model);
		assertTrue(rtnStr.contains("systemData"));
=======
		HttpEntity<String> rtnStr = agentController.getState(0L, "127.0.0.1", "127.0.0.1");
		assertTrue(rtnStr.getBody().contains("freeMemory"));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

}
