/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.monitor.share.domain;

<<<<<<< HEAD
import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

=======
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
/**
 * Network Usage Calculation Class. This class get the bandwidth so far and calc RX, TX.
 * 
 * @author JunHo Yoon
 * 
 */
public class BandWidth implements Serializable {

	/**
	 * UUID.
	 */
	private static final long serialVersionUID = 7655104078722834344L;

	private long time;

	/**
	 * Default constructor.
	 */
<<<<<<< HEAD
=======
	@SuppressWarnings("UnusedDeclaration")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public BandWidth() {
	}

	/**
	 * Constructor with the timestamp.
	 * 
<<<<<<< HEAD
	 * @param time
	 *            current timestamp.
=======
	 * @param time	current timestamp.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public BandWidth(long time) {
		this.time = time;
	}

<<<<<<< HEAD
	private long recieved;
	private long sent;

	private long recivedPerSec;
=======
	private long received;
	private long sent;

	private long receivedPerSec;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private long sentPerSec;

	/**
	 * Calculate the bandWith by subtracting prev bandwidth.
	 * 
<<<<<<< HEAD
	 * @param bandWidth
	 *            bandWidth adjusted against.
=======
	 * @param bandWidth	bandWidth adjusted against.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return adjusted bandWidth.
	 */
	public BandWidth adjust(BandWidth bandWidth) {
		float rate = ((float) Math.abs(time - bandWidth.getTime())) / 1000;
<<<<<<< HEAD
		recivedPerSec = ((long) ((recieved - bandWidth.getRecieved()) * rate));
=======
		receivedPerSec = ((long) ((received - bandWidth.getReceived()) * rate));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		sentPerSec = ((long) ((sent - bandWidth.getSent()) * rate));
		return this;
	}

	public long getTime() {
		return time;
	}

	public long getSentPerSec() {
		return sentPerSec;
	}

	public void setSentPerSec(long sentPerSec) {
		this.sentPerSec = sentPerSec;
	}

<<<<<<< HEAD
	public long getRecivedPerSec() {
		return recivedPerSec;
	}

	public void setRecivedPerSec(long recivedPerSec) {
		this.recivedPerSec = recivedPerSec;
	}

	public long getRecieved() {
		return recieved;
	}

	public void setRecieved(long recieved) {
		this.recieved = recieved;
=======
	public long getReceivedPerSec() {
		return receivedPerSec;
	}

	public void setReceivedPerSec(long receivedPerSec) {
		this.receivedPerSec = receivedPerSec;
	}

	public long getReceived() {
		return received;
	}

	public void setReceived(long received) {
		this.received = received;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	public long getSent() {
		return sent;
	}

	public void setSent(long sent) {
		this.sent = sent;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}