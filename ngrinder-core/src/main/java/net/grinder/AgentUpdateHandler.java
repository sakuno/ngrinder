/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package net.grinder;

<<<<<<< HEAD
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import net.grinder.engine.communication.UpdateAgentGrinderMessage;
import net.grinder.util.NetworkUtil;
import net.grinder.util.VersionNumber;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;
import org.ngrinder.common.util.CompressionUtil;
=======
import net.grinder.communication.CommunicationException;
import net.grinder.engine.communication.AgentUpdateGrinderMessage;
import net.grinder.util.VersionNumber;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.ngrinder.common.constants.AgentConstants;
import org.ngrinder.common.util.CompressionUtils;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.infra.AgentConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

<<<<<<< HEAD
/**
 * Agent Update Message Handler.
 * 
 * @author JunHo Yoon
 * @since 3.1
 */
public class AgentUpdateHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(AgentUpdateHandler.class);

	private final AgentConfig agentConfig;

	/**
	 * Agent Update handler.
	 * 
	 * @param agentConfig
	 *            agentConfig
	 */
	public AgentUpdateHandler(AgentConfig agentConfig) {
		this.agentConfig = agentConfig;
	}

	boolean isNewer(String newVersion, String installedVersion) {
=======
import java.io.*;

import static org.ngrinder.common.constants.InternalConstants.PROP_INTERNAL_NGRINDER_VERSION;
import static org.ngrinder.common.util.Preconditions.checkTrue;

/**
 * Agent Update Message Handler.
 *
 * @author JunHo Yoon
 * @since 3.1
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
public class AgentUpdateHandler implements Closeable {
	private static final Logger LOGGER = LoggerFactory.getLogger(AgentUpdateHandler.class);

	private final AgentConfig agentConfig;
	private File download;
	@SuppressWarnings("FieldCanBeLocal")
	private int offset = 0;
	private FileOutputStream agentOutputStream;

	/**
	 * Agent Update handler.
	 *
	 * @param agentConfig agentConfig
	 * @param message     AgentUpdateGrinderMessage
	 */
	public AgentUpdateHandler(AgentConfig agentConfig, AgentUpdateGrinderMessage message)
			throws FileNotFoundException {
		if (!agentConfig.getAgentProperties().getPropertyBoolean(AgentConstants.PROP_AGENT_UPDATE_ALWAYS)) {
			checkTrue(isNewer(message.getVersion(), agentConfig.getInternalProperties().getProperty(PROP_INTERNAL_NGRINDER_VERSION)),
					"Update request was sent. But it's the older version " + message.getVersion());
		}
		this.agentConfig = agentConfig;
		this.download = new File(agentConfig.getHome().getTempDirectory(), "ngrinder-agent.tar");
		this.agentOutputStream = new FileOutputStream(download);
		LOGGER.info("AgentUpdateHandler is initialized!");
	}

	boolean isNewer(String newVersion, String installedVersion) {
		if (newVersion.contains("-SNAPSHOT")) {
			return true;
		}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		installedVersion = installedVersion.replaceAll("\\(.*\\)", "").trim();
		newVersion = newVersion.replaceAll("\\(.*\\)", "").trim();
		return new VersionNumber(newVersion).compareTo(new VersionNumber(installedVersion)) > 0;
	}

<<<<<<< HEAD
	/**
	 * Update agent based on the current message.
	 * 
	 * @param message
	 *            message to be sent
	 */
	public void updateAgent(UpdateAgentGrinderMessage message) {
		if (!isNewer(message.getVersion(), agentConfig.getInternalProperty("ngrinder.version", "UNKNOWN"))) {
			LOGGER.info("Update request was sent. But the old version was sent");
			return;
		}
		File downloadFolder = new File(System.getProperty("user.dir"), "download");
		downloadFolder.mkdirs();
		File dest = new File(downloadFolder, message.getFileName());

		File interDir = new File(agentConfig.getCurrentDirectory(), "update_package_unzip");
		File updatePackageDir = new File(System.getProperty("user.dir"), "update_package");
		try {
			NetworkUtil.downloadFile(message.getDownloadUrl(), dest);
			uncompress(dest, interDir, updatePackageDir);
			System.exit(10);
		} catch (Exception e) {
			LOGGER.error("Update request was sent. But download was failed {} ", e.getMessage());
			LOGGER.info("Details : ", e);
		}
	}

	void uncompress(File from, File interDir, File toDir) {
		interDir.mkdirs();
		if (FilenameUtils.isExtension(from.getName(), "gz")) {
			File outFile = new File(toDir, "ngrinder-agent.tar");
			CompressionUtil.ungzip(from, outFile);
			CompressionUtil.untar(outFile, interDir);
			FileUtils.deleteQuietly(outFile);
		} else if (FilenameUtils.isExtension(from.getName(), "zip")) {
			CompressionUtil.unzip(from, interDir);
		} else {
			LOGGER.error("{} is not allowed to be unzipped.", from.getName());
		}
		// List up only directories.
		File[] listFiles = interDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return (pathname.isDirectory());
			}
		});
		if (ArrayUtils.isNotEmpty(listFiles)) {
			try {
				FileUtils.deleteQuietly(toDir);
				FileUtils.moveDirectory(listFiles[0], toDir);
			} catch (IOException e) {
				LOGGER.error("Error while moving a file ", e);
			}
		} else {
			LOGGER.error("{} is empty.", interDir.getName());
		}
		FileUtils.deleteQuietly(from);
		FileUtils.deleteQuietly(interDir);

=======
	public void close() {
		IOUtils.closeQuietly(agentOutputStream);
		FileUtils.deleteQuietly(download);
	}

	/**
	 * Update agent based on the current message.
	 *
	 * @param message message to be sent
	 */
	public void update(AgentUpdateGrinderMessage message) throws CommunicationException {
		if (message.getOffset() != offset) {
			throw new CommunicationException("Expected " + offset + " offset," +
					" but " + message.getOffset() + " was sent. " + ToStringBuilder.reflectionToString(message));
		}
		try {
			IOUtils.write(message.getBinary(), agentOutputStream);
			offset = message.getNext();
		} catch (IOException e) {
			throw new CommunicationException("Error while writing binary", e);
		}
		if (message.getNext() == 0) {
			IOUtils.closeQuietly(agentOutputStream);
			decompressDownloadPackage();
			// Then just exist to run the agent update process.
			System.exit(0);
		}
	}

	void decompressDownloadPackage() {
		File interDir = new File(agentConfig.getHome().getTempDirectory(), "update_package_unpacked");
		File toDir = new File(agentConfig.getCurrentDirectory(), "update_package");
		interDir.mkdirs();
		toDir.mkdirs();

		if (FilenameUtils.isExtension(download.getName(), "tar")) {
			File outFile = new File(toDir, "ngrinder-agent.tar");
			CompressionUtils.untar(download, interDir);
			FileUtils.deleteQuietly(outFile);
		} else {
			LOGGER.error("{} is not allowed to be unpacked.", download.getName());
		}

		try {
			FileUtils.deleteQuietly(toDir);
			FileUtils.moveDirectory(new File(interDir, "ngrinder-agent"), toDir);
		} catch (IOException e) {
			LOGGER.error("Error while moving a file ", e);
		}
		FileUtils.deleteQuietly(download);
		FileUtils.deleteQuietly(interDir);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}
}
